import PropTypes from 'prop-types';
import React from 'react';

export default class SearchBar extends React.Component {
  static propTypes = {
    searchQuery: PropTypes.string,
    onChangeQuery: PropTypes.func.isRequired,
    onClickSearch: PropTypes.func.isRequired,
  };
  
  handleQueryChange = e => {
    this.props.onChangeQuery(e.target.value.trim());
  }
  
	render() {
		return(
			<div id="searchbar">
         <input
          type="search"
          value={this.props.searchQuery}
          placeholder="Search for jokes"
          onChange={this.handleQueryChange}
          name="searchQuery" />
         <button onClick={this.props.onClickSearch}>Search</button>
     	</div>
		);
	}
}
