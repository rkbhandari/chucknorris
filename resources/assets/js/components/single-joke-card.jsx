import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import { VOTE_UP, VOTE_DOWN } from './main-component-controller';

export default class SingleJokeCard extends React.Component {
  static propTypes = {
    joke: PropTypes.shape({
      category: PropTypes.arrayOf(PropTypes.string),
      icon_url: PropTypes.string.isRequired,
      id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]).isRequired,
      url: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      vote_up: PropTypes.number,
      vote_down: PropTypes.number,
    }).isRequired,
    onVote: PropTypes.func.isRequired,
    onClickRandomJokes: PropTypes.func.isRequired,
  };
  
  render() {
    const vote_up = this.props.joke.vote_up || 0;
    const vote_down = this.props.joke.vote_down || 0;
    
    return (
      <div className='single-joke-wrapper'>
        <div className="single-joke">
          <div><img src={this.props.joke.icon_url} /></div>
          {this.props.joke.value}
          <div className="voting-booth">
            <span><a 
              className={classNames({
                'like': true,
                'disabled': false
              })}
              title="I like it!"
              onClick={(e) => this.props.onVote(this.props.joke, VOTE_UP)}
              >
              <i className="fas fa-thumbs-up"></i>
            </a>
              <strong>{vote_up}</strong>
            </span>
            <span><a 
              className={classNames({
                'unlike':true,
                'disabled': false
              })}
              title="It's not that good!"
              onClick={(e) => this.props.onVote(this.props.joke, VOTE_DOWN)}
              >
              <i className="fas fa-thumbs-down"></i>
            </a>
              <strong>{vote_down}</strong>
            </span>
          </div>
        </div>
        <button onClick={this.props.onClickRandomJokes}>I want to hear another joke</button>
      </div>
    );
  }
}
