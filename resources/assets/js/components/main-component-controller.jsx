import PropTypes from 'prop-types';
import React from 'react';
import Immutable from 'immutable';
import classNames from 'classnames';

import SearchBar from './search-bar';
import JokesListComponent from './jokes-list-component';
import SingleJokeCard from './single-joke-card';

const CN_API_BASE_URL = 'https://api.chucknorris.io/jokes/';
const LOCAL_SERVER_URL = 'http://chucknorris.dadulab.com/';
const CN_TYPE_SINGLE = 'Random jokes';
const CN_TYPE_LIST = 'List of jokes';
const CN_TYPE_POPULAR = 'List of popular jokes';
const JOKES_LIST_LIMIT = 10;
export const VOTE_UP = 'like';
export const VOTE_DOWN = 'unlike';

export function capitalize(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

export default class MainComponentController extends React.Component {
	state = {
		initialDataLoaded: false,
		contentType: CN_TYPE_POPULAR,
		categories: [],
		singleJoke: null,
		loadingJokes: false,
		activeCategory: null,
		jokesCollection: null,
		searchQuery: '',
		currentPage: 1,
		localJokes: Immutable.OrderedMap(),
	}

	componentDidMount() {
		this.fetchInitialData().then(
			this.fetchLocalJokes()
		);
	}

	fetchInitialData = () => {
		return $.ajax({
			url: CN_API_BASE_URL + 'categories',
			type: 'GET',
			dataType: 'json'
		})
		.done((data, status, jqXHR) => {
			this.setState({
				initialDataLoaded: true,
				categories: data,
			});
		}).fail((data, status, jqXHR) => {
			
		});
	}

	fetchLocalJokes = () => {
		return $.ajax({
			url: LOCAL_SERVER_URL + 'jokes',
			type: 'GET',
			dataType: 'json'
		})
		.done((data, status, jqXHR) => {
			this.updateLocalJokesData(data);
		});
	}
	
	fetchRandomCategoryJokes = category => {
		this.setState({
			loadingJokes: true,
		});
		return $.ajax({
			url: CN_API_BASE_URL + 'random',
			data: {
				category,
			},
			type: 'GET',
			dataType: 'json'
		})
		.done((data, status, jqXHR) => {
			data = this.attachVotesInChuckJoke(data);
			this.setState({
				singleJoke: data,
				contentType: CN_TYPE_SINGLE,
				loadingJokes: false,
			});
		});
	}
	
	handleCategoryClick = (e, category) => {
		e.preventDefault();
		this.setState({
			activeCategory: category,
		});
		this.fetchRandomCategoryJokes(category);
	}
	
	handleQueryChange = (query) => {
		this.setState({
			searchQuery: query
		});
	}
	
	handleSearch = () => {
		if (this.state.searchQuery === '') {
			return false;
		}
		this.setState({
			loadingJokes: true,
			activeCategory: null,
		});
		return $.ajax({
			url: CN_API_BASE_URL + 'search',
			data: {
				query: this.state.searchQuery,
			},
			type: 'GET',
			dataType: 'json'
		})
		.done((data, status, jqXHR) => {
			data = this.updateSearchData(data);
			this.setState({
				jokesCollection: data,
				loadingJokes: false,
				contentType: CN_TYPE_LIST
			});
		});
	}

	handlePageChange = page => {
		this.setState({
			currentPage: page
		});
	}
	
	handleNextRandomJoke = () => {
		this.fetchRandomCategoryJokes(this.state.activeCategory);
	}

	handleVoting = (joke, action) => {
		$.ajax({
			headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
			url: LOCAL_SERVER_URL + 'vote',
			data: {
				joke,
				action
			},
			type: 'POST',
			dataType: 'json',
		})
		.done((data, status, jqXHR) => {
			this.updateJokesData(data);
		});
	}
	
	handleSeePopularJokes = (e) => {
		e.preventDefault();
		this.setState({
			contentType: CN_TYPE_POPULAR,
			activeCategory: null,
		});
	}

	
	// Helper functions
	highlighSearchTerm(jokes) {
		return jokes.map((joke) => {
			// Need to do for both cases as case-insentive check didn't work
			const lowercaseQuery = this.state.searchQuery.toLowerCase();
			joke.value = joke.value.replace(lowercaseQuery, '<strong>'+lowercaseQuery+'</strong>');
			const capitalisedQuery = capitalize(this.state.searchQuery);
			joke.value = joke.value.replace(capitalisedQuery, '<strong>'+capitalisedQuery+'</strong>');
			return joke;
		});
	}
	
	attachVotesInChuckJoke = (joke) => {
		const localVersion = this.state.localJokes.get(joke.id);
		if (localVersion) {
			joke.vote_up = localVersion.vote_up;
			joke.vote_down = localVersion.vote_down;
		}
		return joke;
	}
	
	updateSearchData = (data) => {
		if (data.total > 0) {
			const newResult = data.result.map(this.attachVotesInChuckJoke);
			data.result = newResult;
		}
		return data;
	}

	updateLocalJokesData = (localJokes) => {
		const localJokesMap = Immutable.OrderedMap().withMutations((map) => {
			localJokes.forEach((joke) => {
				joke.icon_url = 'https://assets.chucknorris.host/img/avatar/chuck-norris.png';
				joke.url = 'https://api.chucknorris.io/jokes/' + joke.jokes_id;
				map.set(joke.jokes_id, joke);
			});
		});
		
		this.setState({
			localJokes: localJokesMap,
		})
	}
	
	updateJokesData = (joke) => {
		// Update single joke data
		if (this.state.singleJoke !== null && this.state.singleJoke.id === joke.jokes_id) {
			const sJoke = Object.assign(this.state.singleJoke, {});
			sJoke.vote_up = joke.vote_up;
			sJoke.vote_down = joke.vote_down;
			this.setState({
				singleJoke: sJoke,
			});
		}
		// Update jokes list data
		if (this.state.jokesCollection !== null && this.state.jokesCollection.total > 0) {
			const jCollection = Object.assign(this.state.jokesCollection, {});
			const results = jCollection.result.map((j) => {
				if (j.id === joke.jokes_id) {
					j.vote_up = joke.vote_up;
					j.vote_down = joke.vote_down;
				}
				return j;
			});
			jCollection.result = results;
			this.setState({
				jokesCollection: jCollection,
			});
		}
		// Update local dataset
		const localJokesMap = this.state.localJokes.withMutations((map) => {
			joke.icon_url = 'https://assets.chucknorris.host/img/avatar/chuck-norris.png';
			joke.url = 'https://api.chucknorris.io/jokes/' + joke.jokes_id;
			map.set(joke.jokes_id, joke);
		}).sort((a, b) => {
			const netDiffA = a.vote_up - a.vote_down;
			const netDiffB = b.vote_up - b.vote_down;
			if (netDiffA > netDiffB) {
				return -1;
			} else if(netDiffB > netDiffA) {
				return 1;
			} else if (netDiffA === netDiffB) {
				if (a.vote_up > b.vote_up) {
					return -1;
				} else if (a.vote_up < b.vote_up) {
					return 1;
				} else {
					return 0;
				}
			}
		});
		this.setState({
			localJokes: localJokesMap,
		});
	}
	
	rendercategories = (category) => {
		return <a
			key={category}
			className={classNames({
				active: this.state.activeCategory === category,
			})}
			onClick={(e) => this.handleCategoryClick(e, category)}>
				{category}
			</a>;
	}
	
	renderContent = () => {
		if (this.state.loadingJokes) {
			return <div className="spinner-middle">
				<img src="/assets/images/Gear.svg" />
				<p>Asking Chuck for jokes...</p>
			</div>;
		}
		const contentType = this.state.contentType;
		switch(contentType) {
			case CN_TYPE_SINGLE:
				return this.renderSingleJokesCard();
				break;
			case CN_TYPE_LIST:
				return this.renderMultipleJokesCard();
				break;
			case CN_TYPE_POPULAR:
				return this.renderPopularJokes();
				break;
			default:
				return null;
		}
	}
	
	renderSingleJokesCard = () => {
		if (!this.state.singleJoke) {
			return null;
		}
		return <SingleJokeCard
			joke={this.state.singleJoke}
			onVote={this.handleVoting}
			onClickRandomJokes={this.handleNextRandomJoke} />;
	}
	
	renderMultipleJokesCard = () => {
		if (!this.state.jokesCollection) {
			return null;
		} else if (this.state.jokesCollection.total === 0) {
			return <div className="not-found">
				<p>Oops! Chuck didn't understand your query!!</p> <p>I bet you would like to try again!</p>
			</div>;
		}
		const totalJokes = this.state.jokesCollection.total;
		const numOfPages = Math.ceil(totalJokes/JOKES_LIST_LIMIT);
		const start = (this.state.currentPage - 1) * JOKES_LIST_LIMIT;
		const end = this.state.currentPage * JOKES_LIST_LIMIT;
		const jokesForPage = this.highlighSearchTerm(this.state.jokesCollection.result.slice(start, end));
		const title = `Jokes for searched term "${this.state.searchQuery}"`;
		
		return <JokesListComponent
			title={title}
			jokesList={jokesForPage}
			total={totalJokes}
			limit={JOKES_LIST_LIMIT}
			currentPage={this.state.currentPage}
			onPageChange={this.handlePageChange}
			onVote={this.handleVoting}/>;
	}
	
	renderPopularJokes = () => {
		if (this.state.localJokes.size === 0) {
			return <div className='not-found'>
				<p>There are no popular jokes!</p> <p>Vote some to make one. :)</p>
			</div>;
		}
		const total = this.state.localJokes.size  <= 10 ? this.state.localJokes.size : 10
		const jokesForPage = this.state.localJokes.valueSeq().toArray().slice(0, total);
		return <JokesListComponent
			title='Most popular jokes'
			jokesList={jokesForPage}
			total={total}
			limit={JOKES_LIST_LIMIT}
			currentPage={1}
			onPageChange={() => {}}
			onVote={this.handleVoting}/>;
	}

	render () {
		return (
			<div>
				<div className="wrapper">
				{this.state.initialDataLoaded ? (
					<div>
						<div id="header">
	           	<h1 className="app-title">Jokes from Chuck Norris</h1>
	           	<SearchBar
	           	 	searchQuery={this.state.searchQuery}
	           	 	onChangeQuery={this.handleQueryChange}
	           	 	onClickSearch={this.handleSearch}
	           	 />
	           	<div id="category">
	            	<div> --- Or ---</div>
	            	<div>get a random jokes from following categories</div>
	            		{this.state.categories.map(this.rendercategories)}
	            	</div>
				      </div>
				      {this.state.contentType !== CN_TYPE_POPULAR && (
				      	<div id="popular-jokes"><a href="#" onClick={(e) => this.handleSeePopularJokes(e)}>Top 10 jokes!</a></div>
				      )}
				      <div id="content-wrapper">
				       	{this.renderContent()}
				      </div>
				    </div>
				) : (
					<div id="bodyCentre">
						<div className="spinner-large">
							<img src="/assets/images/Gear.svg" />
						</div>
						<div className="loading"><h1>Loading ...</h1></div>
					</div>
				)}
				</div>
				<div className="footer">Developer: Ram Kumar Bhandari</div>
       </div>
		);
	}
}
