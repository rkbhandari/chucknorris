import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import ReactHtmlParser from 'react-html-parser';

import { VOTE_UP, VOTE_DOWN } from './main-component-controller';

export default class JokesListComponent extends React.Component {
	static propTypes = {
    title: PropTypes.string.isRequired,
		jokesList: PropTypes.arrayOf(PropTypes.shape({
			category: PropTypes.arrayOf(PropTypes.string),
			icon_url: PropTypes.string.isRequired,
			id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]).isRequired,
			url: PropTypes.string.isRequired,
			value: PropTypes.string.isRequired,
		})).isRequired,
		total: PropTypes.number.isRequired,
		limit: PropTypes.number.isRequired,
		currentPage: PropTypes.number.isRequired,
		onPageChange: PropTypes.func.isRequired,
    onVote: PropTypes.func.isRequired,
	};
  
	handlePageClick = (e, i) => {
	    e.preventDefault();
	    this.props.onPageChange(i);
	}

	renderJokesCardForList = joke => {
    const vote_up = joke.vote_up || 0;
    const vote_down = joke.vote_down || 0;
    
		return <div key={joke.id} className='joke-list-card'>
			<div className='icon'>
				<img src={joke.icon_url} />
			</div>
			<div className='joke'><p>{ReactHtmlParser(joke.value)}</p></div>
			<div className={classNames({
				'like-unlike': true,
			})}>
				<p><a 
    			className={classNames({
	    			'like': true,
	    			'disabled': false
	    		})}
	    		title="I like it!"
          onClick={(e) => this.props.onVote(joke, VOTE_UP)}
    			>
    			<i className="fas fa-thumbs-up"></i>
    		</a><strong>{vote_up}</strong></p>
    		<p><a 
    			className={classNames({
	    			'unlike':true,
	    			'disabled': false
	    		})}
	    		title="It's not that good!"
          onClick={(e) => this.props.onVote(joke, VOTE_DOWN)}
	    		>
    			<i className="fas fa-thumbs-down"></i>
    		</a><strong>{vote_down}</strong></p>
			</div>
		</div>;
	}

	renderPagination = () => {
		const numOfPages = Math.ceil(this.props.total/this.props.limit);
		const pageLink = [];
		for(let i = 1; i <= numOfPages; i++) {
			pageLink.push(
				<a
          href="#" 
          className={classNames({
            'currentPage': i === this.props.currentPage
          })}
          key={i}
          onClick={(e) => this.handlePageClick(e, i)}> {i} </a>
			);
		}
		return <div className='pagination'><span>{this.props.total} jokes |</span>{pageLink}</div>;
	}

	render() {
		return (
			<div className='jokes-list-wrapper'>
        <div className='title'>{this.props.title}</div>
				{this.renderPagination()}
				{this.props.jokesList.map(this.renderJokesCardForList)}
        {this.renderPagination()}
			</div>
		);
	}
}
