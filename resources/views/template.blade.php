<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Chuck Norris</title>

        <!-- Fonts -->
        <link href="{{ url('/css/app.css') }}" rel="stylesheet" type="text/css">

        <!-- Styles -->
        
    </head>
    <body>
       <div id="header">
           <h1 class="app-title">Jokes from Chuck Norris</h1>
           <div id="searchbar">
               <input type="search" placeholder="Search for jokes" name="searchQuery" />
               <button>Search</button>
           </div>
           <div id="category">
            <div> --- Or ---</div>
            <div>get a random jokes from following categories</div>
               <a href="#">Category 1</a>
               <a href="#">Category 2</a>
               <a href="#">Category 3</a>
               <a href="#">Category 4</a>
               <a href="#">Category 5</a>
               <a href="#">Category 5</a>
               <a href="#">Category 5</a>
               <a href="#">Category 5</a>
               <a href="#">Category 5</a>
               <a href="#">Category 5</a>
               <a href="#">Category 5</a>
               <a href="#">Category 5</a>
               <a href="#">Category 5</a>
           </div>
       </div>
       <div id="content-wrapper">
           <div></div>
       </div>
    </body>
</html>
