<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Joke;

class JokesController extends Controller
{
	const VOTE_UP = 'like';
	const VOTE_DOWN = 'unlike';

    public function getStoredJokes(Request $request) {
        $jokes = Joke::getOrderedJokes();
    	return response()->json($jokes);
    }

    public function vote(Request $request) {
    	$joke = $request->input('joke');
    	$action = $request->input('action');
        
        $id = array_key_exists('jokes_id', $joke) ? $joke['jokes_id'] : $joke['id'];
    	
    	$jokeModel = Joke::where('jokes_id', $id)->first();
    	if ($jokeModel) {
    		if ($action === self::VOTE_UP) {
    			$jokeModel->vote_up++;
    		} else {
    			$jokeModel->vote_down++;
    		}
    		$jokeModel->save();
    	} else {
    		$jokeModel =  new Joke();
    		$jokeModel->jokes_id = $id;
    		$jokeModel->value = strip_tags($joke['value']);
    		if ($action === self::VOTE_UP) {
    			$jokeModel->vote_up = 1;
    			$jokeModel->vote_down = 0;
    		} else {
    			$jokeModel->vote_up = 0;
    			$jokeModel->vote_down = 1;
    		}
    		$jokeModel->save();
    	}
    	return response()->json($jokeModel);
    }
}
