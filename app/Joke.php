<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Joke extends Model
{
    public static function getOrderedJokes() {
      $jokes = DB::select('select *, vote_up-vote_down as net_diff from jokes order by net_diff DESC, vote_up DESC');
      // $collection = [];
      // foreach ($jokes as $joke) {
      //   $collection[$joke->jokes_id] = $joke;
      // }
      return $jokes;
    }
}
